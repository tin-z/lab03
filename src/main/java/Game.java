class Game {

private int [] punteggio;
private int i;
private int frame;
private boolean isSpare;
private boolean [] isStrike;
private int extrapoints;

	public Game()
	{
		punteggio=new int[]{0,0,0,0,0,0,0,0,0,0};
		this.isSpare=false;
		this.isStrike=new boolean[]{false,false,false,false,false,false,false,false,false,false};
		this.frame=0;
		this.i=0;
		this.extrapoints=0;
	}

	public void roll(int pins){

		if(i>10){
			extrapoints += pins;
			return;
		}



		punteggio[i]= punteggio[i] + pins;

		if((i>1) && (frame==1) && isStrike[i-2])
		{
			punteggio[i-2] = punteggio[i-2] + punteggio[i] + punteggio[i-1];
		}
		if(isSpare && (frame == 1))
		{
			punteggio[i-1]=punteggio[i-1] + punteggio[i];
			isSpare=false;
		}

		if( punteggio[i] == 10)
		{
			if(frame==1)
				isSpare =true;
			else
			{
				isStrike[i]=true;
				frame=1;
			}
		}

		if(frame==0)
		{
			frame=1;
		} else {
			frame=0;
		}

		if(frame== 0)
			i++;

	}
	public int score(){
		int ret=0;
		for( int x : punteggio)
			ret += x;
	
		ret += extrapoints;	
		if(isSpare)
			ret = ret + extrapoints;
		return ret;
	}
}

